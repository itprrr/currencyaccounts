package kamil.linek.assignment.controllers;


import kamil.linek.assignment.domain.entities.Client;
import kamil.linek.assignment.dto.AddClientRequest;
import kamil.linek.assignment.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "/client")
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping(value = "/get/{PESEL}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Client get(@PathVariable("PESEL") String PESEL){
        return clientService.getClientByPESEL(PESEL);
    }

    @PostMapping(value = "/add")
    public Client add(@RequestBody AddClientRequest addClientRequest){
        return clientService.addClient(addClientRequest);
    }

}
