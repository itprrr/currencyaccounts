package kamil.linek.assignment.controllers;


import kamil.linek.assignment.enums.Currency;
import kamil.linek.assignment.services.ExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;


@RestController
@RequestMapping(path = "/exchange")
public class ExchangeController {

    private final ExchangeService exchangeService;

    @Autowired
    public ExchangeController(ExchangeService exchangeService) {
        this.exchangeService = exchangeService;
    }

    @GetMapping(value = "/for/{PESEL}/{amount}/from/{from}/to/{to}")
    public HttpStatus exchange(@PathVariable("PESEL") String PESEL, @PathVariable("amount") BigDecimal amount,
                           @PathVariable("from") Currency from, @PathVariable("to") Currency to){

        this.exchangeService.exchange(PESEL,amount,from,to);
        return HttpStatus.OK;
    }

}
