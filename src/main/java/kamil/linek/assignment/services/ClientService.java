package kamil.linek.assignment.services;


import kamil.linek.assignment.domain.entities.Account;
import kamil.linek.assignment.domain.entities.Client;
import kamil.linek.assignment.domain.repositories.ClientRepository;
import kamil.linek.assignment.dto.AddClientRequest;
import kamil.linek.assignment.enums.Currency;
import kamil.linek.assignment.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@Transactional
@Slf4j
public class ClientService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public Client addClient(AddClientRequest addClientRequest) {
        Client client = convertToClient(addClientRequest);

        client.addToAccounts(createSubAccount(Currency.PLN, addClientRequest.getInitAmount()));
        client.addToAccounts(createSubAccount(Currency.USD, BigDecimal.ZERO));

        return clientRepository.save(client);
    }

    private Account createSubAccount(Currency currency, BigDecimal initAmount){
        Account account = new Account();
        account.setAmount(initAmount);
        account.setCurrency(currency);
        account.setNumber(Utils.generateRandomAccountNumber(24));
        return account;
    }

    private Client convertToClient(AddClientRequest addClientRequest) {
        Client client = new Client();
        client.setFirstName(addClientRequest.getFirstName());
        client.setLastName(addClientRequest.getLastName());
        client.setPESEL(addClientRequest.getPESEL());
        return client;
    }

    public Client getClientByPESEL(String pesel) {
        Client client = clientRepository.getByPESEL(pesel);
        return client;
    }
}
