package kamil.linek.assignment.services;


import kamil.linek.assignment.domain.entities.Account;
import kamil.linek.assignment.domain.entities.Client;
import kamil.linek.assignment.domain.repositories.AccountRepository;
import kamil.linek.assignment.enums.Currency;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
@Transactional
@Slf4j
public class ExchangeService {

    private final AccountRepository accountRepository;

    private final NBPApiService nbpApiService;

    private final ClientService clientService;

    @Autowired
    public ExchangeService(AccountRepository accountRepository, NBPApiService nbpApiService, ClientService clientService) {
        this.accountRepository = accountRepository;
        this.nbpApiService = nbpApiService;
        this.clientService = clientService;
    }

    public void exchange(String PESEL, BigDecimal amount, Currency from, Currency to){
        Client client = clientService.getClientByPESEL(PESEL);

        Account accountFrom = client.getAccounts().stream().filter(t -> t.getCurrency().equals(from)).findAny().get();
        Account accountTo = client.getAccounts().stream().filter(t -> t.getCurrency().equals(to)).findAny().get();

        if(accountFrom.getAmount().compareTo(amount) < 0){
            throw new IllegalArgumentException("Insufficient funds");
        }

        updateAccountsAmount(amount, accountFrom, accountTo);

    }

    private void updateAccountsAmount(BigDecimal amount, Account accountFrom, Account accountTo) {
        accountFrom.setAmount(accountFrom.getAmount().subtract(amount));
        accountRepository.save(accountFrom);

        accountTo.setAmount(accountTo.getAmount().add(convert(amount, accountFrom.getCurrency(), accountTo.getCurrency())));
        accountRepository.save(accountTo);
    }

    private BigDecimal convert(BigDecimal amount, Currency baseCurrencyCode, Currency targetCurrencyCode) {

        BigDecimal baseCurrencyRate = getCurrencyRate(baseCurrencyCode);
        BigDecimal targetCurrencyRate = getCurrencyRate(targetCurrencyCode);

        return amount.multiply(baseCurrencyRate).divide(targetCurrencyRate, 8, RoundingMode.HALF_EVEN);
    }

    private BigDecimal getCurrencyRate(Currency currencyCode) {
        if (Currency.PLN.equals(currencyCode)){
            return BigDecimal.ONE;
        }
        return nbpApiService.getActualMidCurrencyRate(currencyCode);
    }

}
