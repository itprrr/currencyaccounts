package kamil.linek.assignment.services;


import kamil.linek.assignment.configurations.NBPApiConfig;
import kamil.linek.assignment.enums.Currency;
import kamil.linek.assignment.dto.ExchangeRates;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.text.MessageFormat;

@Service
@Transactional
@Slf4j
public class NBPApiService {

    private final NBPApiConfig nbpApiConfig;

    private final RestTemplate restTemplate;

    @Autowired
    public NBPApiService(NBPApiConfig nbpApiConfig, RestTemplate restTemplate) {
        this.nbpApiConfig = nbpApiConfig;
        this.restTemplate = restTemplate;
    }

    public BigDecimal getActualMidCurrencyRate(Currency currency) {
        log.info("Retrieve rate for currency {}", currency);

        final String url = nbpApiConfig.getUrl() + MessageFormat.format(nbpApiConfig.getUri(), currency);

        final ExchangeRates exchangeRates = restTemplate.getForObject(url, ExchangeRates.class);

        log.info("Retrieved exchangeRates {}", exchangeRates);

        return exchangeRates.getRates().stream().findAny().get().getMid();
    }




}
