package kamil.linek.assignment.domain.repositories;

import kamil.linek.assignment.domain.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {

}
