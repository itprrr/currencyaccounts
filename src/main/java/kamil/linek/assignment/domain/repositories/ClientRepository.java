package kamil.linek.assignment.domain.repositories;

import kamil.linek.assignment.domain.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {

    boolean existsByPESEL(String PESEL);

    Client getByPESEL(String PESEL);
}
