package kamil.linek.assignment.domain.entities;

import kamil.linek.assignment.enums.Currency;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;


@Data
@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private Currency currency;

    @DecimalMin(value = "0.0")
    private BigDecimal amount;

    @Column(unique = true)
    private String number;
}
