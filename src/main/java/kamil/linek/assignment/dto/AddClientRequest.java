package kamil.linek.assignment.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode
public class AddClientRequest {
    private String firstName;
    private String lastName;
    private String PESEL;
    private BigDecimal initAmount;
}
