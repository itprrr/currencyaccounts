package kamil.linek.assignment.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class ExchangeRates {
    private String table;
    private String currency;
    private String code;
    private List<Rate> rates;
}
