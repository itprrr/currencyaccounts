package kamil.linek.assignment.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode
public class Rate {
    private LocalDate effectiveDate;
    private BigDecimal mid;
}
