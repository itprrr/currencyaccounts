package kamil.linek.assignment.configurations;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Getter
@Setter
@Configuration
@PropertySource("classpath:api.properties")
public class NBPApiConfig {

    @Value("${nbp.api.url}")
    private String url;

    @Value("${nbp.api.uri.exchangerates}")
    private String uri;
}
