package kamil.linek.assignment.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.math.BigDecimal;
import java.math.MathContext;


public final class Utils {

    private Utils() {
        throw new UnsupportedOperationException("Utils cannot be instantiated");
    }

    public static String generateRandomAccountNumber(int count){
        return RandomStringUtils.randomNumeric(count);
    }

}
