package kamil.linek.assignment.controllers;

import kamil.linek.assignment.AssignmentApplication;
import kamil.linek.assignment.domain.entities.Client;
import kamil.linek.assignment.dto.AddClientRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;

@SpringBootTest(classes = AssignmentApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ExchangeControllerIT {

    public static final String PESEL = "90091204052";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void testExchange() {
        HttpStatus result = restTemplate.getForObject("http://localhost:" + port + "/exchange/for/" + PESEL + "/3/from/PLN/to/USD", HttpStatus.class);
        Assertions.assertTrue(result.equals(HttpStatus.OK));
    }

}
