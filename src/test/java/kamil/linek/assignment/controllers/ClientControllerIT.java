package kamil.linek.assignment.controllers;

import kamil.linek.assignment.AssignmentApplication;
import kamil.linek.assignment.domain.entities.Client;
import kamil.linek.assignment.dto.AddClientRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Repeat;

import java.math.BigDecimal;

@SpringBootTest(classes = AssignmentApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ClientControllerIT {

    public static final String PESEL = "90091204052";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void testAdd() {
        AddClientRequest addClientRequest = new AddClientRequest();
        addClientRequest.setFirstName("dsa");
        addClientRequest.setLastName("ddd");
        addClientRequest.setPESEL(PESEL);
        addClientRequest.setInitAmount(BigDecimal.TEN);

        Client client = restTemplate.postForObject("http://localhost:" + port + "/client/add", addClientRequest, Client.class);
        Assertions.assertEquals(client.getPESEL(), addClientRequest.getPESEL());
    }

    @Test
    void testGet() {
        String result = restTemplate.getForObject("http://localhost:" + port + "/client/get/" + PESEL, String.class);
        Assertions.assertTrue(result.contains(PESEL));
    }
}
